#Example 1: Define a function that takes an argument. Call the function. Identify what code is the argument and what code is the parameter.
def sum(a, b):
    x = a + b
    return x
a=1
b=3
sum(1,3)

#Example 2: Call your function from Example 1 three times with different arguments: a value, a variable, and an expression. Identify which kind of argument is which. 
a=1
b=3
print((1*1)*(3*3))

#Example 3: Create a function with a local variable. Show what happens when you try to use that variable outside the function. Explain the results.
def add(Number1,Number2):
    sumTotal = Number1 + Number2      
    return sumTotal
sumTotal = 20             
print(f"Total = {sumTotal}")

#Example 4: Create a function that takes an argument. Give the function parameter a unique name. Show what happens when you try to use that parameter name outside the function. Explain the results.
def P(a=4,b=2):
  return a+b
print(a+2)

#Example 5: Show what happens when a variable defined outside a function has the same name as a local variable inside a function. Explain what happens to the value of each variable as the program runs.
def Phuong(a,b=4):
  return a+b
a=6
print(a)