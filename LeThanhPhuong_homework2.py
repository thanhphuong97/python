#bài 1:
a = input("Enter name: ")
b = int(input("Enter age: "))
c = 70 - b
print(c,"this year you will turn 70 years old")

#bài 2:
s = input("Please enter the string in English:")
print(s.swapcase())

#bài 3:
d = 'Machine learning for Covid-19 diagnosis'
print(len(d))
print("chain d:" , d.isdigit())
print(d.upper())
print(d[0:4])
print(d.replace(" ",""))
print(d.split(","))

#bài 4:
name = input("Enter name: ")
age = int(input("Enter age: "))
countryside = input("Enter address: ")
home = input("Enter hometown: ")
inf ="Hello, My name is {}. I am {} years old. I come from {} city. I live at {}"
print(inf.format(name,age,countryside,home))
